// By default, the client will authenticate using the service account file
// specified by the GOOGLE_APPLICATION_CREDENTIALS environment variable and use
// the project specified by the GOOGLE_CLOUD_PROJECT environment variable. See
// https://github.com/GoogleCloudPlatform/google-cloud-node/blob/master/docs/authentication.md
// These environment variables are set automatically on Google App Engine
import { Storage } from '@google-cloud/storage';
import { format } from 'util';

// Instantiate a storage client

const uploadFile = async (file) => {
  const storage = new Storage();
  const bucket = storage.bucket(process.env.GCLOUD_STORAGE_BUCKET);
  const fileName = file.name;
  const blob = bucket.file(fileName);
  const blobStream = blob.createWriteStream();
  return new Promise((resolve, reject) => {
    try{    
      if (!file) {
        return reject({ status: 404, message : 'File not found !'});
      }
      if(!bucket.name){
        return reject({ status: 404, message : 'bucket name not found !'});
      }
                
      blobStream.on('error', (err) => {
        return reject({ error: err, message : 'File not found !'});
      });
  
      blobStream.on('finish', () => {
        // The public URL can be used to directly access the file via HTTP.
        const publicUrl = format(
          `https://storage.googleapis.com/${bucket.name}/${blob.name}`
        );
        blobStream.end(file.buffer);
        return resolve({ url: publicUrl, status: 200, message: 'file get uploaded successfully'});
      });
    } catch(error) {
      return reject({ status: 500, message : 'file upload get failed!', error: error});
    }
  });
}

export { uploadFile };